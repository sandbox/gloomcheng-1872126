<?php
namespace Drupal\addressfield_tw;

use Drupal\mixin\Traits\Hook;

class Format {
  use Hook;
  protected $format;
  protected $address;
  protected $context;
  protected $parents = [];

  /** @var Address */
  protected $helper;
  protected $isModeForm;
  protected $wrapperIndetity;

  function __construct(&$format, $address, $context = []) {
    $this->format = &$format;
    $this->address = $address + ['region' => '', 'wrapper_last' => ''];
    $this->context = $context;
    $this->isModeForm = ('form' == $this->context['mode']);
    $this->wrapperIndetity = ($this->isModeForm) ? $format['#wrapper_id'] : '';
    $this->helper = Address::create();
    $this->setupParents();
    $this->setupRegion();
  }

  protected function setupParents() {
    $parents = [];
    $items = ['region', 'administrative_area', 'locality'];

    foreach ($items as $parent) {
      foreach ($items as $item) {
        $parents[$parent][] = $item;

        if ($parent == $item) {
          break;
        }
      }
    }

    $this->parents = $parents;
  }

  protected function setupRegion() {
    $address = &$this->address;

    if ('' != $address['region']) {
      return;
    }

    $index = $address['administrative_area'];
    $items = $this->helper->getAdministrativeArea();

    if (isset($items[$index])) {
      $address['region'] = $items[$index]->getParent()->getCode();
    }
  }

  static function hook_addressfield_format_callback(&$format, $address, $context = []) {
    $item = new static($format, $address, $context);
    return $item->doFormatTaiwn();
  }

  function doFormatTaiwn() {
    $this->formWrapperLast();
    $this->formLocalityBlock();
    $this->formRegion();
    $this->formAdministrativeArea();
    $this->formLocality();
    $this->formPostalCode();
    $this->formStreetBlock();
    $this->formThoroughfare();
    $this->formPremise();
    return;
  }

  protected function formWrapperLast() {
    $format = &$this->format;
    $index = 'wrapper_last';
    $format[$index] = ['#typet' => 'hidden', '#default_value' => '', '#value' => ''];
  }

  protected function formLocalityBlock() {
    $format = &$this->format;
    $index = 'locality_block';

    if (isset($format[$index])) {
      $format[$index] = ['#weight' => 1] + $format[$index];    
    }
  }

  protected function formRegion() {
    $block = &$this->format['locality_block'];
    $index = 'region';

    $block[$index] = [
        '#title' => t('Region'),
        '#weight' => 0,
      ] + $this->formAddressSelect($index);
  }

  protected function formAddressSelect($item) {
    $identity = $this->wrapperIndetity;
    $options = $this->getOptions($item);

    $form = [
      '#default_value' => $this->address[$item],
      '#options' => $options,
      '#render_option_value' => TRUE,
      '#required' => TRUE,
    ];

    if (FALSE == $this->isModeForm) {
      return $form;
    }

    $form += [
      '#type' => 'select',
      '#wrapper_id' => $identity,
      '#process' => ['ajax_process_form', 'addressfield_tw_format_ajax_process_form'],
      '#ajax' => [
        'callback' => 'addressfield_standard_widget_refresh',
        'wrapper' => $identity,
        'method' => 'replace',
      ]
    ];

    return $form;
  }

  protected function getOptions($item) {
    $address = &$this->address;
    $left = $this->helper->getRoot();
    $options = [];

    foreach ($this->parents[$item] as $parent) {
      $options = $left->getChildrenArray();
      $index = $address[$parent];

      if (FALSE == isset($options[$index])) {
        $keys = array_keys($options);
        $index = $address[$parent] = reset($keys);
      }

      $children = $left->getChildren();
      $left = $children[$index];
    }

    return $options;
  }

  protected function formAdministrativeArea() {
    $block = &$this->format['locality_block'];
    $index = 'administrative_area';
    $block[$index] = [
        '#title' => t('City'),
        '#weight' => 1,
      ] + $this->formAddressSelect($index) + $block[$index];
  }

  protected function formLocality() {
    $block = &$this->format['locality_block'];
    $index = 'locality';
    $block[$index] = [
        '#title' => t('District'),
        '#weight' => 2,
      ] + $this->formAddressSelect($index) + $block[$index];
  }

  protected function formPostalCode() {
    $block = &$this->format['locality_block'];
    $address = &$this->address;
    $value = $address['postal_code'] = $address['locality'];
    $index = 'postal_code';
    $block[$index] = [
        '#value' => $value,
        '#disabled' => TRUE,
        '#weight' => 3,
      ] + $block[$index];
  }

  protected function formStreetBlock() {
    $format = &$this->format;
    $index = 'street_block';
    $format[$index] = ['#weight' => 2] + $format[$index];
  }

  protected function formThoroughfare() {
    $block = &$this->format['street_block'];
    $index = 'thoroughfare';
    $block[$index] = [
        '#title' => t('Address'),
        '#default_value' => $this->address[$index],
      ] + $block[$index];
  }

  protected function formPremise() {
    $block = &$this->format['street_block'];
    $index = 'premise';

    $block[$index] = [
        '#access' => FALSE
      ] + $block[$index];
  }

}
