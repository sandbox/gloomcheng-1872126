<?php

namespace Drupal\addressfield_tw;

use Drupal\mixin\Traits\Object;

class Area {
  use Object;
  protected $code;
  protected $title;

  /** @var self */
  protected $parent;

  /** @var self[] */
  protected $children = [];

  function __construct($code, $title, &$parent = NULL) {
    $this->code = $code;
    $this->title = $title;
    $this->parent =& $parent;
  }

  function isRoot() {
    $flag = (FALSE == isset($this->parent));
    return $flag;
  }

  function hasChildern() {
    $flag = (FALSE == empty($this->children));
    return $flag;
  }

  function getParent() {
    return $this->parent;
  }

  function setParent(self &$parent = NULL) {
    unset($this->parent);
    $this->parent = &$parent;
    return $this;
  }

  function getChildren() {
    return $this->children;
  }

  function setChildren(array $children = []) {
    $this->children = $children;
    return $this;
  }

  function getChildrenArray() {
    $items = [];

    foreach ($this->children as $item) {
      $code = $item->getCode();
      $items[$code] = $item->getTitle();
    }

    return $items;
  }

  function getCode() {
    return $this->code;
  }

  function setCode($code) {
    $this->code = $code;
    return $this;
  }

  function getTitle() {
    return $this->title;
  }

  function setTitle($title) {
    $this->title = $title;
    return $this;
  }
}
