<?php

namespace Drupal\addressfield_tw;

use Drupal\mixin\Caller;
use Drupal\mixin\Getter;
use Drupal\mixin\Traits\Object;

class Address {
  use Object;
  /** @var Area */
  protected $root;
  /** @var Area[] */
  protected $region = [];
  /** @var Area[] */
  protected $administrative_area = [];
  /** @var Area[] */
  protected $locality = [];
  protected $data = [];
  protected $language;

  protected function __construct() {
    global $language;
    $this->language = $language->language;
    $this->loadData();
    $this->setupRoot();
  }

  protected function loadData() {
    module_load_include('inc', 'phpexcel');
    $path = drupal_realpath(drupal_get_path('module', 'addressfield_tw') . '/src/fixtures');
    $items = ['region', 'administrative_area', 'locality'];

    foreach ($items as $item) {
      $file = $path . '/' . $item . '.xls';
      $this->data[$item] = phpexcel_import($file, TRUE, TRUE);
    }
  }

  protected function setupRoot() {
    $area = new Area('', '');
    $area->setChildren($this->setupRegion($area));
    $this->root = $area;
  }

  protected function setupRegion($root) {
    $items = [];
    $data = $this->fetchData(['region', 'Root']);

    foreach ($data as $item) {
      $code = $item['code'];
      $area = new Area($code, $this->fetchTitle($item), $root);
      $items[$code] = $this->region[$code] = $area;
      $area->setChildren($this->setupAdministrativeArea($item));
    }

    return $items;
  }

  protected function fetchData(array $indexes = []) {
    $data = Getter::create($this->data, $indexes)->setBuilder(Caller::create(NULL, []))->fetch(FALSE);
    return $data;
  }

  protected function fetchTitle($item) {
    $order = [$this->language, 'en', 'code'];

    foreach ($order as $index) {
      if (isset($item[$index])) {
        return t($item[$index]);
      }
    }

    return '';
  }

  protected function setupAdministrativeArea($parent) {
    $items = [];
    $data = $this->fetchData(['administrative_area', $parent['en']]);
    $object = $this->region[$parent['code']];

    foreach ($data as $item) {
      $code = $item['code'];
      $area = new Area($code, $this->fetchTitle($item), $object);
      $items[$code] = $this->administrative_area[$code] = $area;
      $area->setChildren($this->setupLocality($item));
    }

    return $items;
  }

  protected function setupLocality($parent) {
    $items = [];
    $data = $this->fetchData(['locality', $parent['en']]);
    $object = $this->administrative_area[$parent['code']];

    foreach ($data as $item) {
      $code = $item['code'];
      $area = new Area($code, $this->fetchTitle($item), $object);
      $items[$code] = $this->locality[$code] = $area;
    }

    return $items;
  }

  static function create() {
    static $cache;

    if (FALSE == isset($cache)) {
      $cache = new static();
    }

    return $cache;
  }

  function getRoot() {
    return $this->root;
  }

  function getRegion() {
    return $this->region;
  }

  function getAdministrativeArea() {
    return $this->administrative_area;
  }

  function getLocality() {
    return $this->locality;
  }

}
