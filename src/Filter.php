<?php

namespace Drupal\addressfield_tw;

use views_handler_filter_in_operator as Base;

class Filter extends Base {
  static function className() {
    return get_called_class();
  }

  function extra_options_form(&$form, &$form_state) {
    parent::extra_options_form($form, $form_state);

    $form['country'] = [
      '#type' => 'select',
      '#title' => t('Country selection form element'),
      '#default_value' => $this->options['country'],
      '#options' => _addressfield_country_options_list(),
    ];
  }

  function get_value_options() {
    if (isset($this->value_options)) {
      return NULL;
    }

    module_load_include('inc', 'addressfield', 'addressfield.administrative_areas');
    $items = addressfield_get_administrative_areas($this->options['country']);
    $this->value_options = isset($items) ? $items : [];
    return $this->value_options;
  }

  function has_extra_options() {
    return TRUE;
  }

  function option_definition() {
    $options = parent::option_definition();
    $list = _addressfield_country_options_list();
    $options['country'] = ['default' => key($list)];
    return $options;
  }

}
