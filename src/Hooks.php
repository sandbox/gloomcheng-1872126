<?php
namespace Drupal\addressfield_tw;

use Drupal\mixin\Traits\Hook;

class Hooks {
  use Hook;

  static function hook_ctools_plugin_directory($module, $plugin) {
    if ($module == 'addressfield' && (false == empty($plugin))) {
      return 'plugins/' . $plugin;
    }

    return null;
  }

  static function hook_format_ajax_process_form($element, &$form_state) {
    $element['#limit_validation_errors'] = [$element['#parents']];
    return $element;
  }

  static function hook_field_attach_form($entity_type, $entity, &$form, &$form_state, $langcode) {
    if ('field_collection_item' != $entity_type) {
      return;
    }

    if (false == $form['#required']) {
      $form['#validated'] = true;
    }
  }

  static function hook_addressfield_administrative_areas_alter(&$administrative_areas) {
    $items = [];

    foreach (Address::create()->getAdministrativeArea() as $index => $item) {
      $items[$index] = $item->getTitle();
    }

    $administrative_areas['TW'] = $items;
  }

  static function hook_views_api() {
    return ['api' => 3];
  }

  static function hook_field_views_data_alter(&$result, $field, $module) {
    if ('addressfield' != $module) {
      return;
    }

    foreach ($result as $table => $setting) {
      $area = $setting['field_address_administrative_area'];
      $suffix = ' - ' . t('Select');
      $area['title'] .= $suffix;
      $area['title short'] .= $suffix;
      $filter = &$area['filter'];
      $filter['handler'] = Filter::className();
      $result[$table]['field_address_administrative_area_select'] = $area;
    }

  }

  protected static function getHookMap() {
    $class = get_called_class();
    $module = static::getType()->getModule();
    $hooks = [];
    $items = [
      'format_ajax_process_form',
      //'field_attach_form',
      'addressfield_administrative_areas_alter',
      'views_api',
    ];

    foreach ($items as $item) {
      $hooks[$class]['hook_' . $item] = $module . '_' . $item;
    }

    return $hooks;
  }

}
