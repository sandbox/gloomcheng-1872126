<?php

use Drupal\addressfield_tw\Format;

$plugin = [
  'title' => t('Address form (Taiwan districts with postal code add-on)'),
  'format callback' => 'addressfield_tw_addressfield_format_callback',
  'type' => 'address',
  'weight' => -80,
];

function addressfield_tw_addressfield_format_callback(&$format, $address, $context = []) {
  if ('TW' == $address['country']) {
    Format::hook_addressfield_format_callback($format, $address, $context);
  }
}

